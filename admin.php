<?php
    ob_start();
    session_start();

    if (isset($_SESSION['superadmin'])){
        header('Location : superadmin.php');
    }
    
    include ('./parts/headerAdmin.html');
    
    require "./classes/Autoloader.php";
    Autoloader::autoload();
    $form = new Form();
    $db = new DB();
    $bootstrap = new Bootstrap();
    echo $bootstrap -> navbarConnected();
    
    if (isset($_POST['titre']) && isset($_POST['contenu'])){
        $db -> addNews($_POST['titre'], $_POST['contenu']);
    }
    
    if (isset($_GET['delete']) && $_GET['delete'] != null){
        $db -> deleteElement($_GET['delete']);
    }
    if ($_SESSION['admin']){
        include './parts/afficherNews.php';
        include './parts/ajouterNews.php';
    } else {
        
    }

?>