<?php
/**
 * TODO: Email 'Nous COntacter'
 * TODO: Inverser sortie News (date: +récent à +ancien)
 */
    session_start();
    include './parts/header.html';
    include './parts/svgs.html';
    
    require './classes/Autoloader.php';
    Autoloader::autoload();
    
    # Constructs
    $form = new Form();
    $db = new DB();
    $bootstrap = new Bootstrap();
    
    # Team Vars
    $delta = $db -> getTeam("delta");
    $sigma = $db -> getTeam("sigma");
    $theta = $db -> getTeam("theta");
    $omega = $db -> getTeam("omega");


    $db -> verifyConnexion();
    $db -> disconnectClient();    
    ?>
    <?php 
        if (isset($_SESSION['admin']) && $_SESSION['admin']){
            echo $bootstrap -> navbarConnected();
        } else {
            echo $bootstrap -> navbarDisconnected();
        }
    ?>
    <div class="container my-4 teamCardContainer">
        <div class="row">
            <div class="col-md-3 teamCard">
                <?=$bootstrap -> carteTeam("Delta", $delta['point_team']);?>
            </div>
            <div class="col-md-3 teamCard">
                <?=$bootstrap -> carteTeam("Sigma", $sigma['point_team']);?>
            </div>
            <div class="col-md-3 teamCard">
                <?=$bootstrap -> carteTeam("Theta", $theta['point_team']);?>
            </div>
            <div class="col-md-3 teamCard">
                <?=$bootstrap -> carteTeam("Omega", $omega['point_team']);?>
            </div>
            
        </div>
    </div>
    <?php include './news.php'?>

    <script src="js/changeBG.js"></script>
    <script src="js/newsCarouselActive.js"></script>

    <?php include './parts/footer.html';?>
