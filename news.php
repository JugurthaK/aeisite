<?php

    $news = $db -> getNewsIndex();

?>

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="10000">
    <div class="carousel-inner">
            <?php foreach ($news as $new) {
                echo ($bootstrap -> carteNewsIndex($new['titre'], $new['contenu'], $new['date_news'], $new['team_name']));
            }?>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>   