function changeBG() {
    var arrayBG = [ 'url("./img/AEIC-TeamCardBG1.jpg")',
                    'url("./img/AEIC-TeamCardBG2.jpg")',
                    'url("./img/AEIC-TeamCardBG3.jpg")',
                    'url("./img/AEIC-TeamCardBG4.jpg")',
                    'url("./img/AEIC-TeamCardBG5.jpg")'
                ];
    var background = arrayBG[Math.floor(Math.random() * arrayBG.length)];
    document.querySelectorAll(".svgClipped").forEach(element => {
        element.style.backgroundImage = background;
    });
    document.querySelector(".homeBody").style.backgroundImage = background;
}

changeBG();