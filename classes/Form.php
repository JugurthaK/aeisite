<?php

    class Form{

        public function __construct(){

        }

        public function label($name, $label){
            return '<label class="control-label" for="'.$name.'">'.$label.'</label>';
        }
        public function input($name){
            return '<input class="form-control my-2" type="text" name="'.$name.'">';
        }
        
        public function inputPassword($name){
            return '<input class="form-control my-2" type="password" name="'.$name.'">';
        }

        public function textArea($name){
            return '<textarea class="form-control my-2" name="'.$name.'" rows="3"></textarea>';
        }

        public function submit(){
            return '<button class="mt-2 btn btn-outline-primary btn-block btn-lg" type="submit">Envoyer</button>';
        }

        public function disconnect(){
            return '<button class="btn-outline-danger" type="submit">Déconnexion</button>';
        }


    }

?>