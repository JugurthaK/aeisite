<?php

    class Bootstrap{
        public function __construct(){

        }
        #TODO:Parametre nomTeam
        public function carteTeam($nomTeam, $nombrePoint){ #<img src="'.$logoTeam.'">
            return '<div class="card">
            <div class="card-body text-center">
              <div class="svgClipped '.$nomTeam.'"></div>
              <h1 class="card-text text-center textTeam my-2 font-weight-bold">'.$nombrePoint.'<span class="textPoint font-italic font-weight-light">Pts</span></h1>
            </div>
          </div>';
        }

        public function navbarDisconnected(){
          return '<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
           <a class="navbar-brand" href="./index.php">
            <img src="img/AEIC-Icon.svg" class="navbar-icon" alt="AEIC">
           </a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
           </button>
           <div class="collapse navbar-collapse justify-content-between" id="navbarNavAltMarkup">
               <div class="navbar-nav">
                   <a class="nav-item nav-link active" href="./index.php">Accueil <span class="sr-only">(current)</span></a>
                   <a class="nav-item nav-link" target="_blank" href="https://shop.aeic.online">Shop</a>
                   <a class="nav-item nav-link target="_blank" href="https://concours.aeic.online">Concours</a>
                </div>
               <a href="connexion.php" class="btn btn-outline-success">Connexion</a>
           </div>
       </nav>';
       }

       public function navbarConnected(){
        return '<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <a class="navbar-brand" href="./index.php">
            <img src="img/AEIC-Icon.svg" class="navbar-icon" alt="AEIC">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-between" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="./index.php">Accueil <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" target="_blank" href="https://shop.aeic.online">Shop</a>
                <a class="nav-item nav-link target="_blank" href="https://concours.aeic.online">Concours</a>
                <a class="nav-item nav-link text-primary" href="./admin.php">Panel Admin</a>
            </div>
            <a href="./index.php?disconnect=true" class="btn btn-outline-danger">Déconnexion</a>
        </div>
    </nav>';
       }

       public function carteNews($titre, $contenu, $date, $team, $id){
           return '
           <div class="col-md-2">
            <div class="card mt-4" style="width: 12rem;">
            <div class="card-body">
                <h5 class="card-title">'.$titre.'</h5>
                <h6 class="card-subtitle mb-2 text-muted fontCardNews">'.ucfirst($team).' / '.$date.'</h6>
                <hr>
                <p class="card-text">'.$contenu.'</p>
                <a class="card-text text-center text-danger" href="?delete='.$id.'"> <i class="fas fa-ban"></i></a>
            </div>
            </div> 
         </div>';
       }
       
       public function carteNewsIndex($titre, $contenu, $date, $team){
           return '
           <div class="carousel-item">
            <div class="col-md-2">
                <div class="card mt-2" style="width: 12rem;">
                    <div class="card-body">
                        <div class="row">
                            <div class="p-2 col-2 newsBadge '.$team.'">
                                <h5 class="card-title font-weight-bold '.$team.'">'.$titre.'</h5>
                                <h6 class="card-subtitle mb-2 text-muted fontCardNews">'.$team.' / '.$date.'</h6>
                            </div>
                            <div class="p-2 col-10 newsTextArea">
                                <p class="card-text text-justify">'.$contenu.'</p>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
         </div>';
       }
    }