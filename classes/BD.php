<?php



class DB{
    
    private $host;
    private $bdd;
    private $user;
    private $pw;

    public function __construct(){
        if (getenv('SERVER_ADDR') == "82.165.80.95"){
            require './config/parameters.php';
            $this -> host = $parameters['host'];
            $this -> bdd = $parameters['bdd'];
            $this -> user = $parameters['user'];
            $this -> pw = $parameters['pw']; 
        } else {   
            require './config/parameters.local.php';
            $this -> host = $parameters['host'];
            $this -> bdd = $parameters['bdd'];
            $this -> user = $parameters['user'];
            $this -> pw = $parameters['pw'];
        }
    }

    public function getPDO(){
        return new PDO("mysql:host=$this->host;dbname=$this->bdd;charset=utf8", $this->user, $this->pw);
    }

    public function getTeam($team){
        $pdo = $this -> getPDO();
        $r = $pdo -> prepare('select * from team where nom_team like :team');
        $r -> bindParam(':team', $team);
        $r -> execute();
        return $r -> fetch();
    }

    /**
     * Obsolète : Modéle à récupérer pour insertion de données
     * @param nom_team : Nom de l'équipe à créer
     */
    private function creationChefMaison($nom_team){
        $pdo = $this -> getPDO();
        $pass = password_hash('123', PASSWORD_DEFAULT);
        $r = $pdo -> prepare("INSERT INTO chef_maison(password, prenom) VALUES (:pass, '{$nom_team}')");
        $r -> bindParam(':pass', $pass);
        $r -> execute();
        var_dump($r);
    }

    /**
     * @param prenom : Permet de récupérer le mot de passe hashé lié à un compte
     * @return string Le mot de passe hashé
     * Inclue dans la fonction VerifyConnexion, pour ça qu'elle est privée
     */
    private function getPasswordHash($prenom){
        $pdo = $this->getPDO();
        $r = $pdo -> prepare("SELECT password FROM chef_maison WHERE prenom like :prenom");
        $r -> bindParam(':prenom', $prenom);
        $r -> execute();
        $hash = $r -> fetch();
        return $hash['password'];
    }

    /**
     * @param password : Mot de passe rentré par l'utilisateur
     * @param hash : Mot de passe hashé
     * @return boolean pour savoir si le mot de passe est correct
     * Inclue dans la fonction VerifyConnexion
     */
    private function verifPassword($password, $hash){
        return password_verify($password, $hash);
    }

    /**
     * @param prenom : Nom de l'utilisateur qui veut se connecter
     * @param password : Mot de passe de l'utilisateur
     */
    public function verifyConnexion(){
        if (isset($_POST['account']) && isset($_POST['password'])){
            if($this->verifPassword($_POST['password'], 
            $this -> getPasswordHash($_POST['account']))){
                if ($_POST['account'] === 'Jugurtha'){
                    $_SESSION['superadmin'] = true;
                }
                $_SESSION['admin'] = true;
                $_SESSION['account'] = $_POST['account'];
                return true;     
            } 
        }
        return false;
    }

    public function addNews($titre, $contenu){
        $titre = htmlspecialchars($titre);
        $contenu = htmlspecialchars($contenu);
        $dateNews = new DateTime();
        $dateNews = $dateNews -> format("d m Y");
        $pdo = $this -> getPDO();
        $r = $pdo -> prepare("INSERT INTO team_news(titre,team_name,contenu,date_news) VALUES (:titre, :team_name, :contenu, :date_news)");
        $r -> bindParam(":titre", $titre);
        $r -> bindParam(":team_name", $_SESSION['account']);
        $r -> bindParam(":contenu", $contenu);
        $r -> bindParam(":date_news", $dateNews);
        $r -> execute();
    }

    public function getNews($nomTeam){
        $pdo = $this -> getPDO();
        $r = $pdo -> prepare("SELECT * FROM team_news WHERE team_name LIKE :team");
        $r -> bindParam(":team", $nomTeam);
        $r -> execute();
        return $r -> fetchAll(PDO::FETCH_ASSOC);
        
    }

    public function disconnectClient(){
        if (isset($_GET['disconnect']) && $_GET['disconnect'] == true){
            session_unset();
        }
    }

    public function deleteElement($id){
            $id = (int) $id;
            $pdo = $this -> getPDO();
            $r = $this->getPDO() -> prepare("SELECT team_name from team_news WHERE id_news = :id");
            $r -> bindParam(":id", $id);
            $r -> execute();
            $name = $r -> fetchColumn();
            
            if (strtolower($name) == strtolower($_SESSION['account'])){
                $r = $this->getPDO() -> prepare("DELETE FROM team_news WHERE id_news = :id");
                $r -> bindParam(':id', $id);
                $r -> execute();
            }

    }

    public function getNewsIndex(){
        $pdo = $this -> getPDO();
        $r = $pdo -> prepare("SELECT * FROM team_news");
        $r -> execute();
        return $r -> fetchAll(PDO::FETCH_ASSOC);
    }

    public function teamToJSON(){
        $pdo = $this -> getPDO();
        $r = $pdo -> prepare("SELECT * from team");
        $r -> execute();
        $resultat = $r -> fetchAll(PDO::FETCH_ASSOC);
        
        return $resultat = json_encode($resultat);  
    }

    public function decodeJSON($json){
        json_decode($json);
    }
}

?>