<?php
    session_start();
    require 'classes/Autoloader.php';
    Autoloader::autoload();
    
    $db = new DB();
    $form = new Form();
    $bootstrap = new Bootstrap();

    $news = $db -> getNewsIndex();

    if (isset($_SESSION['superadmin'])){
        echo "{$_SESSION['account']} Vous êtes co en SU";
    }else {
        die();
    }
    
    if (isset($_POST['titre']) && isset($_POST['contenu'])){
        $db -> addNews($_POST['titre'], $_POST['contenu']);
    }
    
    if (isset($_GET['delete']) && $_GET['delete'] != null){
        $db -> deleteElement($_GET['delete']);
    }

?>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AEIC</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body class="homeBody">
<div class="container">
    <div class="row">
        <?php foreach ($news as $new) {
            echo ($bootstrap -> carteNews($new['titre'], $new['contenu'], $new['date_news'], $new['team_name'], $new['id_news']));
        }?>
    </div>
</div> 

<?php

        require './parts/ajouterNews.php';

?>