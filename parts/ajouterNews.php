<div class="container">
    <hr>
    <div class="row">
        <div class="offset-md-4 col-md-4">
            <form action="#" method="POST" class="form-group">
                <?= $form -> label("titre", "Titre News");?>
                <?= $form -> input("titre");?>
                <?= $form -> label("contenu", "Contenu News");?>
                <?= $form -> textArea("contenu")?>
                <?= $form -> submit(); ?>
            </form>

        </div>
    </div>
</div>