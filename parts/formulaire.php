<body>
    <div class="container">
        <div class="row">
            <div class="offset-md-4 col-md-4 mt-4 text-center">
                <a href="./index.php">
                    <img src="./img/AEIC-Favicon.svg" width="80%">
                </a>
            </div>
            <div class="offset-md-4 col-md-4 connexionCenterPage">
                <h3 class="text-center text-primary">Connexion Admin</h3>
                <form action="#" method="POST" class="form-group text-center">
                    <?= $form -> label("account", "Nom de Compte");?>
                    <?= $form -> input("account");?>
                    <?= $form -> label("password", "Mot de Passe");?>
                    <?= $form -> inputPassword("password");?>
                    <?= $form -> submit(); ?>
                </form>
                
            </div>
        </div>
    </div>
</body>