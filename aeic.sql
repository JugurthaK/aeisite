-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 11, 2018 at 07:18 PM
-- Server version: 5.7.22-0ubuntu0.17.10.1
-- PHP Version: 7.1.17-0ubuntu0.17.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aeic`
--

-- --------------------------------------------------------

--
-- Table structure for table `chef_maison`
--

CREATE TABLE `chef_maison` (
  `id_personne` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `prenom` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chef_maison`
--

INSERT INTO `chef_maison` (`id_personne`, `password`, `prenom`) VALUES
(8, '$2y$10$1jW/sHuNYJCDvCwmenZ0gufqnAKpyC6wqCK47c3YT.krobXWs0r6i', 'Delta'),
(9, '$2y$10$kw8WbdtoHvC2U6ymVo19L.QG9P26YptWFzbGI00c/zJRNxcSgxPQm', 'Sigma'),
(10, '$2y$10$mwPuKAMb7UsyKzpX5wJkSO.IZdQeTcyo44d.vNkO/6eM8WghWbFNW', 'Théta'),
(11, '$2y$10$l/giAzQqqeKA0.LSAxT/E.nrPwUolp0D0p3Cn/sVrxNdptVQZpKw.', 'Oméga');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id_team` int(11) NOT NULL,
  `nom_team` text NOT NULL,
  `point_team` int(11) NOT NULL,
  `logo_team` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id_team`, `nom_team`, `point_team`, `logo_team`) VALUES
(1, 'Delta', 127, ''),
(2, 'Sigma', 178, ''),
(3, 'Théta', 151, ''),
(4, 'Oméga', 140, '');

-- --------------------------------------------------------

--
-- Table structure for table `team_news`
--

CREATE TABLE `team_news` (
  `id_news` int(11) NOT NULL,
  `titre` text NOT NULL,
  `team_name` text NOT NULL,
  `contenu` text NOT NULL,
  `date_news` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chef_maison`
--
ALTER TABLE `chef_maison`
  ADD PRIMARY KEY (`id_personne`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id_team`);

--
-- Indexes for table `team_news`
--
ALTER TABLE `team_news`
  ADD PRIMARY KEY (`id_news`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chef_maison`
--
ALTER TABLE `chef_maison`
  MODIFY `id_personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id_team` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `team_news`
--
ALTER TABLE `team_news`
  MODIFY `id_news` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
