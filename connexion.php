<?php
    ob_start();
    include './parts/header.html';
    
    require './classes/Autoloader.php';
    Autoloader::autoload();
    Init::initialize();

    $db = new DB();
    $form = new Form();

    if ($db -> verifyConnexion()){
        header('Location: admin.php');
        exit();
    } else {
        include './parts/formulaire.php';
    }
?>
    

<?php include './parts/footer.html'?>